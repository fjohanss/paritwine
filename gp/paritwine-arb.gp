install ("pari_acb_exp", "Gb", "acb_exp", LIBPARITWINESO);
install ("pari_acb_log", "Gb", "acb_log", LIBPARITWINESO);
install ("pari_acb_atan", "Gb", "acb_atan", LIBPARITWINESO);
install ("pari_acb_sin", "Gb", "acb_sin", LIBPARITWINESO);
install ("pari_acb_cos", "Gb", "acb_cos", LIBPARITWINESO);
install ("pari_acb_gamma", "Gb", "acb_gamma", LIBPARITWINESO);
install ("pari_acb_digamma", "Gb", "acb_digamma", LIBPARITWINESO);
install ("pari_acb_zeta", "Gb", "acb_zeta", LIBPARITWINESO);
install ("pari_acb_hurwitz_zeta", "GGb", "acb_hurwitz_zeta", LIBPARITWINESO);
install ("pari_acb_modular_eta", "Gb", "acb_modular_eta", LIBPARITWINESO);
install ("pari_acb_modular_j", "Gb", "acb_modular_j", LIBPARITWINESO);
install ("pari_acb_elliptic_p", "GGb", "acb_elliptic_p", LIBPARITWINESO);
install ("pari_acb_elliptic_inv_p", "GGb", "acb_elliptic_inv_p", LIBPARITWINESO);
install ("pari_acb_elliptic_zeta", "GGb", "acb_elliptic_zeta", LIBPARITWINESO);
install ("pari_acb_elliptic_sigma", "GGb", "acb_elliptic_sigma", LIBPARITWINESO);

install ("pari_fmpz_numbpart", "G", "fmpz_numbpart", LIBPARITWINESO);
install ("pari_arb_numbpart", "Gb", "arb_numbpart", LIBPARITWINESO);

